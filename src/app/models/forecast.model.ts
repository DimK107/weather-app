export class SearchResponse {
    cod: string;
    count: number;
    message: string;
    list: SearchCityListItem[];
}

export class SearchCityListItem {
    id: number;
    name: string;
    wind: {
        speed: number;
    };
    main: {
        humidity: number;
        pressure: number;
        temp: number;
        temp_max: number;
        temp_min: number;
    };
}

export class CityWeatherForecast {
    cod: string;
    cnt: number;
    message: number;
    list: Forecast[];
    city: City;
}

export class Forecast {
    dt: number;
    dt_txt: string;
    clouds: {
        all: number;
    };
    wind: {
        deg: number;
        speed: number;
    };
    main: {
        humidity: number;
        pressure: number;
        temp: number;
    };
}

export class City {
    id: number;
    name: string;
}
