export class ChartDataItem {
    name?: string;
    value?: number;
}

export class ChartAreaItem {
    name?: string;
    series?: ChartDataItem[];
}

export class ChartLineItem {
    name?: string;
    series?: ChartDataItem[];
}
