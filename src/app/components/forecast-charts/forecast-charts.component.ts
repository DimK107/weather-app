import { Component, OnInit } from '@angular/core';
import { ChartAreaItem, ChartDataItem, ChartLineItem } from '../../models/chart.model';
import { WeatherService } from '../../services/weather.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { CityWeatherForecast, Forecast } from '../../models/forecast.model';

@Component({
  selector: 'app-forecast-charts',
  templateUrl: './forecast-charts.component.html',
  styleUrls: ['./forecast-charts.component.scss']
})
export class ForecastChartsComponent implements OnInit {
  chartColorScheme = {
    domain: ['#ffc5d0']
  };

  windChartData: ChartLineItem[];
  temperatureChartData: ChartDataItem[];
  pressureChartData: ChartAreaItem[];
  humidityChartData: ChartLineItem[];

  cityWeather$: Observable<CityWeatherForecast>;
  isWeatherLoading$: Observable<boolean>;

  constructor(private weather: WeatherService) { }

  ngOnInit() {
    this.cityWeather$ = this.weather.cityWeather;

    this.cityWeather$.subscribe(data => {
      if (data) { this.setChartsData(data); }
    });
  }

  setChartsData(data: CityWeatherForecast) {

    const daysForecast = new Map<string, Forecast>();

    data.list.forEach((forecast: Forecast) => {
      const day = moment(forecast.dt * 1000).format('D MMM');
      if (!daysForecast.has(day)) {
        daysForecast.set(day, forecast);
      }
    });

    const temperatureChartData: ChartDataItem[] = [];
    const windChartData: ChartLineItem[] = [{
      name: 'Wind',
      series: []
    }];
    const pressureChartData: ChartAreaItem[] = [{
      name: 'Pressure',
      series: []
    }];
    const humidityChartData: ChartLineItem[] = [{
      name: 'Humidity',
      series: []
    }];

    daysForecast.forEach((forecast, dayName) => {
      temperatureChartData.push({ name: dayName, value: forecast.main.temp });
      windChartData[0].series.push({ name: dayName, value: forecast.wind.speed });
      pressureChartData[0].series.push({ name: dayName, value: forecast.main.pressure });
      humidityChartData[0].series.push({ name: dayName, value: forecast.main.humidity });
    });

    this.windChartData = windChartData;
    this.temperatureChartData = temperatureChartData;
    this.pressureChartData = pressureChartData;
    this.humidityChartData = humidityChartData;
  }
}
