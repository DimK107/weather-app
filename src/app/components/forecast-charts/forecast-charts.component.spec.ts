import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastChartsComponent } from './forecast-charts.component';

describe('ForecastChartsComponent', () => {
  let component: ForecastChartsComponent;
  let fixture: ComponentFixture<ForecastChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
