import { SearchResponse, SearchCityListItem, CityWeatherForecast } from '../models/forecast.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class WeatherService {

  private cityWeather$: BehaviorSubject<CityWeatherForecast> = new BehaviorSubject(null);
  private isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient) { }

  get cityWeather(): Observable<CityWeatherForecast> {
    return this.cityWeather$.asObservable();
  }

  get isLoading(): Observable<boolean> {
    return this.isLoading$.asObservable();
  }

  searchCities(term): Observable<SearchCityListItem[]> {
    return this.http.get<SearchResponse>(`https://api.openweathermap.org/data/2.5/find?q=${term}&zip=${term}&units=metric&appid=${environment.weatherAPIKEY}`)
      .pipe(map(res => res.list));
  }

  loadCityWeather(id: number): void {
    this.isLoading$.next(true);
    this.http.get<CityWeatherForecast>(`https://api.openweathermap.org/data/2.5/forecast?id=${id}&units=metric&appid=${environment.weatherAPIKEY}`)
      .subscribe(res => {
        this.isLoading$.next(false);
        this.cityWeather$.next(res);
      });
  }
}
