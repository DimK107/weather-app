import { SearchCityListItem, CityWeatherForecast } from './models/forecast.model';
import { WeatherService } from './services/weather.service';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError, filter, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  todayDate = new Date();

  cityWeather$: Observable<CityWeatherForecast>;
  isWeatherLoading$: Observable<boolean>;

  cities$: Observable<SearchCityListItem[]>;
  citiesLoading = false;
  citiesSearch$ = new Subject<string>();
  selectedCity: SearchCityListItem;

  constructor(private weather: WeatherService) { }

  ngOnInit() {
    this.cityWeather$ = this.weather.cityWeather;
    this.isWeatherLoading$ = this.weather.isLoading;
    this.loadCities();
  }

  private loadCities() {
    this.cities$ = concat(
      of([]),
      this.citiesSearch$.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        filter(term => !!term),
        switchMap(term => {
          this.citiesLoading = true;
          return this.weather.searchCities(term)
            .pipe(
              catchError((err) => of([])),
              finalize(() => this.citiesLoading = false)
            );
        })
      )
    );
  }

  onSelectCity(city: SearchCityListItem) {
    if (city) {
      this.weather.loadCityWeather(city.id);
    }
  }
}
